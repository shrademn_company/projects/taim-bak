//------------------------------------------------------------------------------
//
// HardpointWidget.cpp created by Yyhrs 2024-06-19
//
//------------------------------------------------------------------------------

#include <QFileDialog>

#include "HardpointWidget.hpp"

HardpointWidget::HardpointWidget(QString const &path, QWidget *parent):
	QWidget{parent}
{
	setupUi(this);
	connect(addPushButton, &QPushButton::clicked, this, [this, path]
	{
		QString file{QFileDialog::getOpenFileName(this, {}, path, tr("Alamo (*.ALO)"))};

		if (!file.isEmpty())
			load(file);
	});
	connect(pathLineEdit, &QLineEdit::textChanged, this, &HardpointWidget::load);
}

const QSharedPointer<Alamo::Model> &HardpointWidget::model() const
{
	return m_model;
}

int HardpointWidget::bone() const
{
	return boneComboBox->currentIndex();
}

void HardpointWidget::load(QString const &path)
{
	pathLineEdit->setText(path);
	boneComboBox->clear();
	m_model.reset(nullptr);
	if (!QFile::exists(path))
		return ;
	m_model.reset(new Alamo::Model{path});
	if (m_model->error().isNull())
		for (auto const &bone: m_model->bones())
			boneComboBox->addItem(bone.name);
}
