#-------------------------------------------------
#
# Project created by Shrademn
#
#-------------------------------------------------

QT += core gui widgets concurrent
TEMPLATE = app
CONFIG += c++17
DEFINES += QT_IMPLICIT_QFILEINFO_CONSTRUCTION

DESTDIR = $$PWD/build
TARGET = TaimBak
TEMPLATE = app

VERSION = 1.0.0.0
RC_ICONS = $${TARGET}.ico
QMAKE_TARGET_COMPANY = "Shrademn'Company"
QMAKE_TARGET_PRODUCT = "✭"
QMAKE_TARGET_DESCRIPTION = $${TARGET}
QMAKE_TARGET_COPYRIGHT = "☭"

SOURCES += main.cpp \
	HardpointWidget.cpp \
	MainWindow.cpp

HEADERS += \
	HardpointWidget.hpp \
	MainWindow.hpp

FORMS += \
    HardpointWidget.ui \
    MainWindow.ui

RESOURCES += resources.qrc

include(alamo/Alamo.pri)
include(core/Core.pri)
include(widget/WidgetEnhanced.pri)
include(widget/WidgetMeta.pri)
