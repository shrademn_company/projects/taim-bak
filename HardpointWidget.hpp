//------------------------------------------------------------------------------
//
// HardpointWidget.hpp created by Yyhrs 2024-06-19
//
//------------------------------------------------------------------------------

#ifndef HARDPOINTWIDGET_HPP
#define HARDPOINTWIDGET_HPP

#include <Models.hpp>

#include "ui_HardpointWidget.h"

class HardpointWidget: public QWidget, private Ui::HardpointWidget
{
	Q_OBJECT

public:
	explicit HardpointWidget(QString const &path, QWidget *parent = nullptr);

	QSharedPointer<Alamo::Model> const &model() const;
	int                                bone() const;

private:
	void load(QString const &path);

	QSharedPointer<Alamo::Model> m_model{nullptr};
};

#endif // HARDPOINTWIDGET_HPP
