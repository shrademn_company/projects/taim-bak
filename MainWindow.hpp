//------------------------------------------------------------------------------
//
// MainWindow.hpp created by Yyhrs 2023/05/31
//
//------------------------------------------------------------------------------

#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QCheckBox>
#include <QMovie>
#include <QTemporaryDir>

#include <Models.hpp>
#include <Settings.hpp>

#include "ui_MainWindow.h"

class MainWindow: public QMainWindow, private Ui::MainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = nullptr);
	~MainWindow();

private:
	enum Type
	{
		Bone = 0,
		Proxy,
		Shader
	};
	enum Column
	{
		Hardpoint = 0,
		Attachment,
		ColumnCount
	};
	enum Role
	{
		Index = Qt::UserRole + 1,
		Data
	};

	void connectActions();
	void connectWidgets();

	void load();
	void cookModel(QSharedPointer<Alamo::Model> &model) const;
	void save(QString const &path);
	void refreshBones();

	Settings                     m_settings;
	QMovie                       m_turbolaser{":/turbolaser.gif"};
	QSharedPointer<Alamo::Model> m_model{nullptr};
	QTemporaryDir                m_preview;
};

#endif // MAINWINDOW_HPP
