//------------------------------------------------------------------------------
//
// main.cpp created by Yyhrs 2023/05/31
//
//------------------------------------------------------------------------------

#include <SApplication.hpp>

#include "MainWindow.hpp"

int main(int argc, char *argv[])
{
    SApplication application{argc, argv};
    MainWindow   window;

    window.show();
    return SApplication::exec();
}
