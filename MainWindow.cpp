//------------------------------------------------------------------------------
//
// MainWindow.cpp created by Yyhrs 2023/05/31
//
//------------------------------------------------------------------------------

#include <QDesktopServices>
#include <QFileDialog>

#include <core.tpp>
#include <Models.hpp>
#include <operators.hpp>
#include <SApplication.hpp>
#include <utils.hpp>

#include "HardpointWidget.hpp"
#include "MainWindow.hpp"

MainWindow::MainWindow(QWidget *parent):
	QMainWindow{parent}
{
	sApp->setTheme(m_settings.value(SApplication::Theme, Theme::c_dark).toString());
	sApp->startSplashScreen(Logos::c_shrademnCompanyRed.arg(Theme::c_light));
	setupUi(this);
	movieLabel->setMovie(&m_turbolaser);
	m_settings.restoreState(this);
	connectActions();
	connectWidgets();
	sApp->stopSplashScreen(this);
	if (sApp->arguments().count() == 2)
		inputPathLineEdit->setText(sApp->arguments().constLast());
}

MainWindow::~MainWindow()
{
	m_settings.saveState(this);
}

void MainWindow::connectActions()
{
	addAction(actionCycleTheme);
	connect(actionCycleTheme, &QAction::triggered, this, &SApplication::cycleTheme);
}

void MainWindow::connectWidgets()
{
	connect(inputPushButton, &QPushButton::clicked, this, [this]
	{
		inputPathLineEdit->openBrowser(PathLineEdit::File, tr("Alamo (*.ALO)"));
	});
	connect(inputPathLineEdit, &PathLineEdit::textChanged, this, &MainWindow::load);
	connect(reloadPushButton, &QPushButton::clicked, this, &MainWindow::load);
	connect(previewPushButton, &QPushButton::clicked, this, [this]
	{
		save(m_preview.filePath(QUuid::createUuid().toString(QUuid::WithoutBraces) + ".ALO"));
	});
	connect(savePushButton, &QPushButton::clicked, this, [this]
	{
		save(QFileDialog::getSaveFileName(this, tr("Save"), inputPathLineEdit->text(), tr("Alamo (*.ALO)")));
	});
}

void MainWindow::load()
{
	stackedWidget->setCurrentIndex(1);
	m_turbolaser.stop();
	editorGroupBox->setEnabled(false);
	m_model.reset(nullptr);
	if (!QFile::exists(inputPathLineEdit->text()))
		return ;
	m_model.reset(new Alamo::Model{inputPathLineEdit->text()});
	if (m_model->error().isNull())
	{
		refreshBones();
		editorGroupBox->setEnabled(true);
	}
	else
	{
		stackedWidget->setCurrentIndex(0);
		m_turbolaser.start();
	}
}

void MainWindow::cookModel(QSharedPointer<Alamo::Model> &model) const
{
	for (int row{0}; row < tableWidget->rowCount(); ++row)
	{
		auto *widget{qobject_cast<HardpointWidget *>(tableWidget->cellWidget(row, Attachment))};

		if (widget && widget->model())
		{
			auto shift{model->bones().count()};
			int  attachment{widget->bone()};

			for (auto bone: widget->model()->bones())
			{
				if (bone.index == attachment)
					bone.parent = row;
				else
					bone.parent += shift;
				bone.index += shift;
				model->bones().append(bone);
			}
			for (auto mesh: widget->model()->meshes())
			{
				mesh.index = model->meshes().count();
				for (auto &subMesh: mesh.subMeshes)
					subMesh.mesh = mesh.index;
				mesh.bone += shift;
				model->connections() << Alamo::Model::Connection{mesh.index, mesh.bone};
				model->meshes().append(mesh);
			}
		}
	}
}

void MainWindow::save(QString const &path)
{
	QSharedPointer<Alamo::Model> model;

	if (path.isNull())
		return ;
	model.reset(new Alamo::Model{inputPathLineEdit->text()});
	cookModel(model);
	model->save(path, scaleDoubleSpinBox->value());
	if (model->error().isNull())
		QDesktopServices::openUrl({path});
	else
	{
		stackedWidget->setCurrentIndex(0);
		m_turbolaser.start();
	}
}

void MainWindow::refreshBones()
{
	QString path{QFileInfo{inputPathLineEdit->text()}.absolutePath()};

	tableWidget->setRowCount(m_model->bones().count());
	for (auto const &bone: m_model->bones())
	{
		tableWidget->setVerticalHeaderItem(bone.index, new QTableWidgetItem{QString::number(bone.index)});
		tableWidget->setItem(bone.index, Hardpoint, new QTableWidgetItem{bone.name});
		tableWidget->setCellWidget(bone.index, Attachment, new HardpointWidget{path});
	}
}
